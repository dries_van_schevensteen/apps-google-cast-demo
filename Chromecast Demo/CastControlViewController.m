//
//  CastControlViewController.m
//  Chromecast Demo
//
//  Created by Dries Van Schevensteen on 07/04/16.
//  Copyright © 2016 Dries Van Schevensteen. All rights reserved.
//

#import "CastControlViewController.h"

#import "GCDWebServer.h"
#import "GCDWebServerDataResponse.h"
#import "GCDWebServerFileResponse.h"
#import "GCDWebServerStreamedResponse.h"



#pragma mark - CastControlViewController class extension -

@interface CastControlViewController () <GCKDeviceManagerDelegate, GCKMediaControlChannelDelegate, GCDWebServerDelegate>

@property (nonatomic, strong) GCDWebServer* webServer;

@property (nonatomic, readonly) NSString* movieUrl;
@property (nonatomic, readonly) NSString* fullMovieName;
@property (nonatomic, readonly) NSString* movieName;
@property (nonatomic, readonly) NSString* movieType;

// Points in time Cast needs to pause
@property (nonatomic, strong) NSArray* pausePoints;
@property (nonatomic, assign) NSInteger currentPausePoint;
@property (nonatomic, assign) BOOL didInitPausePoint;

@end



#pragma mark - CastControlViewController implementation -

@implementation CastControlViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    _movieName = @"BigBuckBunny";
    _movieType = @"mp4";
    
    self.deviceManager.delegate = self;
    self.mediaControlChannel.delegate = self;
    
    _pausePoints = @[[NSNumber numberWithDouble:10.0],
                     [NSNumber numberWithDouble:20.0],
                     [NSNumber numberWithDouble:30.0],
                     [NSNumber numberWithDouble:40.0],
                     [NSNumber numberWithDouble:50.0]];
    _currentPausePoint = 0;
    
    [GCDWebServer setLogLevel:2];
    [self startSever];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.webServer stop];
}


#pragma mark Custom getters & setters

-(NSString *)movieUrl
{
    // online video: @"https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
    // to test locally: download file and name it BigBuckBunny.mp4
    
    return [NSString stringWithFormat:@"%@", self.webServer.serverURL];
}

-(NSString *)fullMovieName
{
    return [NSString stringWithFormat:@"%@.%@", _movieName, _movieType];
}



#pragma mark Server

-(void)startSever
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:self.movieName ofType:self.movieType];
    
    _webServer = [[GCDWebServer alloc] init];
    _webServer.delegate = self;
    
    [_webServer addDefaultHandlerForMethod:@"GET"
                              requestClass:[GCDWebServerRequest class]
                              processBlock:^GCDWebServerResponse *(GCDWebServerRequest *request)
    {
        NSRange dataRange = [request byteRange];
        return [GCDWebServerFileResponse responseWithFile:filePath byteRange:dataRange];
    }];
    
    [_webServer startWithPort:8080 bonjourName:nil];
}



#pragma mark - CGDWebServer delegate methods -

-(void)webServerDidStart:(GCDWebServer *)server
{
    NSLog(@"Server did start");
}

-(void)webServerDidStop:(GCDWebServer *)server
{
    NSLog(@"Server did stop");
}



#pragma mark Actions

-(IBAction)castVideo:(id)sender
{
    if (!_deviceManager || _deviceManager.connectionState != GCKConnectionStateConnected || !self.webServer.isRunning)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Not connected" message:@"Select device first" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* action = [UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        GCKMediaMetadata* metaData = [[GCKMediaMetadata alloc] init];
        
        [metaData setString:@"Chromecast demo test video" forKey:kGCKMetadataKeyTitle];
        [metaData setString:@"Moovly" forKey:kGCKMetadataKeySubtitle];
        
        GCKMediaInformation* mediaInformation = [[GCKMediaInformation alloc] initWithContentID:self.movieUrl
                                                                                    streamType:GCKMediaStreamTypeNone
                                                                                   contentType:@"video/mp4"
                                                                                      metadata:metaData
                                                                                streamDuration:0
                                                                                   mediaTracks:nil
                                                                                textTrackStyle:GCKMediaTextTrackStyleEdgeTypeNone
                                                                                    customData:nil];

        [self.mediaControlChannel loadMedia:mediaInformation autoplay:YES playPosition:0];
    }
}

-(IBAction)play:(id)sender
{
    [self.mediaControlChannel play];
    
    self.didInitPausePoint = NO;
}

-(IBAction)pause:(id)sender
{
    [self.mediaControlChannel pause];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (IBAction)stop:(id)sender
{
    [self.mediaControlChannel stop];
}

-(IBAction)back:(id)sender
{
    NSTimeInterval seekToTime = MAX(0, self.mediaControlChannel.approximateStreamPosition - 4);
    [self.mediaControlChannel seekToTimeInterval:seekToTime];
}

-(IBAction)next:(id)sender
{
    NSTimeInterval seekToTime = self.mediaControlChannel.approximateStreamPosition + 4;
    [self.mediaControlChannel seekToTimeInterval:seekToTime];
}



#pragma mark - GCKMediaControlChannelDelegate delegate methods

-(void)mediaControlChannelDidUpdateStatus:(GCKMediaControlChannel *)mediaControlChannel
{
    NSLog(@"Did update status: %ld", mediaControlChannel.mediaStatus.playerState);

    if (mediaControlChannel.mediaStatus.playerState == GCKMediaPlayerStatePlaying)
        [self initFirstPausePoint];
}

-(void)initFirstPausePoint
{
    if (!self.didInitPausePoint && (self.pausePoints.count > self.currentPausePoint))
    {
        double currentPoint = [[self.pausePoints objectAtIndex:self.currentPausePoint] doubleValue];
        
        NSTimeInterval currentTime = self.mediaControlChannel.approximateStreamPosition;
        if (!(currentTime > currentPoint))
        {
            [self performSelector:@selector(pause:) withObject:nil afterDelay:currentPoint - currentTime];
            [self performSelector:@selector(seekToTime:) withObject:[NSNumber numberWithDouble:currentPoint] afterDelay:currentPoint - currentTime];
        }
        
        self.currentPausePoint++;
    }
    
    self.didInitPausePoint = YES;
}

-(void)seekToTime:(NSNumber*)interval
{
    [self.mediaControlChannel seekToTimeInterval:[interval doubleValue]];
}

-(void)mediaControlChannel:(GCKMediaControlChannel *)mediaControlChannel didCompleteLoadWithSessionID:(NSInteger)sessionID
{
    NSLog(@"Did complete load");
}

-(void)mediaControlChannel:(GCKMediaControlChannel *)mediaControlChannel requestDidCompleteWithID:(NSInteger)requestID
{
    NSLog(@"Request did complete");
}

@end
