//
//  DevicesTableViewController.m
//  Chromecast Demo
//
//  Created by Dries Van Schevensteen on 06/04/16.
//  Copyright © 2016 Dries Van Schevensteen. All rights reserved.
//

#import "DevicesTableViewController.h"
#import "CastControlViewController.h"



#pragma mark - DevicesTableViewController class extension -

@interface DevicesTableViewController () <GCKDeviceScannerListener, GCKDeviceManagerDelegate, GCKMediaControlChannelDelegate>

@property (strong, nonatomic) GCKDeviceManager* deviceManager;
@property (strong, nonatomic) GCKMediaControlChannel* mediaControlChannel;

@end



#pragma mark - DevicesTableViewController implementation -

@implementation DevicesTableViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.deviceScanner addListener:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showCastControls"])
    {
        UIViewController* viewController = segue.destinationViewController;
        if([viewController isKindOfClass:[CastControlViewController class]])
        {
            ((CastControlViewController*)viewController).deviceManager = self.deviceManager;
            ((CastControlViewController*)viewController).mediaControlChannel = self.mediaControlChannel;
        }
    }
}



#pragma mark Acitons

-(void)updateUI
{
    if (!_deviceManager || _deviceManager.connectionState != GCKConnectionStateConnected)
    {
        self.castVideoButton.enabled = NO;
    }
    else
    {
        self.castVideoButton.enabled = YES;
    }
}



#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.deviceScanner.devices.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GCKDevice* device = [self.deviceScanner.devices objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellReuseIdentifier" forIndexPath:indexPath];
    
    cell.textLabel.text = device.friendlyName;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GCKDevice* device = [self.deviceScanner.devices objectAtIndex:indexPath.row];
    NSLog(@"Did select device with name %@!", device.friendlyName);
    
    if (self.deviceManager)
        [self.deviceManager disconnect];
    
    _deviceManager = [[GCKDeviceManager alloc] initWithDevice:device clientPackageName:[NSBundle mainBundle].bundleIdentifier];
    _deviceManager.delegate = self;
    [_deviceManager connect];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark GCKDeviceScannerListener delegate methods

-(void)deviceDidComeOnline:(GCKDevice *)device
{
    NSLog(@"Device did come online %@", device.friendlyName);
    [self.tableView reloadData];
}

-(void)deviceDidChange:(GCKDevice *)device
{
    NSLog(@"Device did change %@", device.friendlyName);
    [self.tableView reloadData];
}

-(void)deviceDidGoOffline:(GCKDevice *)device
{
    NSLog(@"Device did go offline %@", device.friendlyName);
    [self.tableView reloadData];
}



#pragma mark - GCKDeviceManagerDelegate delegate methods

-(void)deviceManagerDidConnect:(GCKDeviceManager *)deviceManager
{
    [self.deviceManager launchApplication:kGCKMediaDefaultReceiverApplicationID];
}

-(void)deviceManager:(GCKDeviceManager *)deviceManager
didConnectToCastApplication:(GCKApplicationMetadata *)applicationMetadata
           sessionID:(NSString *)sessionID
 launchedApplication:(BOOL)launchedApplication
{
    NSLog(@"Connected and launched: %d", launchedApplication);
    
    _mediaControlChannel = [[GCKMediaControlChannel alloc] init];
    _mediaControlChannel.delegate = self;
    [self.deviceManager addChannel:_mediaControlChannel];
    
    [self updateUI];
}

-(void)deviceManager:(GCKDeviceManager *)deviceManager didDisconnectWithError:(NSError *)error
{
    NSLog(@"Disconnected with error: %@", error);
    [self updateUI];
}



#pragma mark - GCKMediaControlChannelDelegate delegate methods

-(void)mediaControlChannel:(GCKMediaControlChannel *)mediaControlChannel didCompleteLoadWithSessionID:(NSInteger)sessionID
{
    
}

-(void)mediaControlChannel:(GCKMediaControlChannel *)mediaControlChannel requestDidCompleteWithID:(NSInteger)requestID
{
    
}



@end
