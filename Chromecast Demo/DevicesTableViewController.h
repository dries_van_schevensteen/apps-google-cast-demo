//
//  DevicesTableViewController.h
//  Chromecast Demo
//
//  Created by Dries Van Schevensteen on 06/04/16.
//  Copyright © 2016 Dries Van Schevensteen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleCast/GoogleCast.h>



#pragma mark - DevicesTableViewController interface -

@interface DevicesTableViewController : UITableViewController



#pragma mark Outlets

@property (weak, nonatomic) IBOutlet UIBarButtonItem *castVideoButton;



#pragma mark Properties

@property (strong, nonatomic) GCKDeviceScanner* deviceScanner;

@end
