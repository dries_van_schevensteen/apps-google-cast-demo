//
//  ViewController.h
//  Chromecast Demo
//
//  Created by Dries Van Schevensteen on 06/04/16.
//  Copyright © 2016 Dries Van Schevensteen. All rights reserved.
//

#import <UIKit/UIKit.h>



#pragma mark - ViewController interface -

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *connectedDevicesLabel;
@property (weak, nonatomic) IBOutlet UIButton *showDevicesButton;

@end

