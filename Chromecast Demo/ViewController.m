//
//  ViewController.m
//  Chromecast Demo
//
//  Created by Dries Van Schevensteen on 06/04/16.
//  Copyright © 2016 Dries Van Schevensteen. All rights reserved.
//

#import "ViewController.h"
#import "DevicesTableViewController.h"

#import <GoogleCast/GoogleCast.h>



NSString* const kApplicationID = @"BE13B491";



#pragma mark - ViewController class extension -

@interface ViewController () <GCKDeviceScannerListener>

@property (nonatomic, strong) GCKDeviceScanner* deviceScanner;

@end



#pragma mark - ViewController implementation -

@implementation ViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    GCKFilterCriteria* filterCriteria = [GCKFilterCriteria criteriaForAvailableApplicationWithID:kApplicationID];

    _deviceScanner = [[GCKDeviceScanner alloc] initWithFilterCriteria:filterCriteria];
    [_deviceScanner addListener:self];
    [_deviceScanner startScan];
    
    [self updateUIForDevices];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showDevices"])
    {
        UIViewController* viewController = segue.destinationViewController;
        if ([viewController isKindOfClass:[DevicesTableViewController class]])
        {
            ((DevicesTableViewController*)viewController).deviceScanner = self.deviceScanner;
        }
    }
}



#pragma mark Actions

-(void)updateUIForDevices
{
    if (self.deviceScanner.devices.count == 0)
    {
        self.showDevicesButton.hidden = YES;
        self.connectedDevicesLabel.text = @"no devices connected";
    }
    else
    {
        self.showDevicesButton.hidden = NO;
        self.connectedDevicesLabel.text = [NSString stringWithFormat:@"%ld device%@ connected",
                                           self.deviceScanner.devices.count,
                                           self.deviceScanner.devices.count == 1 ? @"" : @"s"];
    }
}



#pragma mark GCKDeviceScannerListener delegate methods

-(void)deviceDidComeOnline:(GCKDevice *)device
{
    NSLog(@"Device did come online %@", device.friendlyName);
    [self updateUIForDevices];
}

-(void)deviceDidChange:(GCKDevice *)device
{
    NSLog(@"Device did change %@", device.friendlyName);
    [self updateUIForDevices];
}

-(void)deviceDidGoOffline:(GCKDevice *)device
{
    NSLog(@"Device did go offline %@", device.friendlyName);
    [self updateUIForDevices];
}

@end
