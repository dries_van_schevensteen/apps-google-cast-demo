//
//  AppDelegate.h
//  Chromecast Demo
//
//  Created by Dries Van Schevensteen on 06/04/16.
//  Copyright © 2016 Dries Van Schevensteen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

