//
//  CastControlViewController.h
//  Chromecast Demo
//
//  Created by Dries Van Schevensteen on 07/04/16.
//  Copyright © 2016 Dries Van Schevensteen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleCast/GoogleCast.h>



#pragma mark - CastControlViewController interface -

@interface CastControlViewController : UIViewController

@property (strong, nonatomic) GCKDeviceManager* deviceManager;
@property (strong, nonatomic) GCKMediaControlChannel* mediaControlChannel;

@end
